/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Javi
 */
public class CalculadoraTest {
    
    public CalculadoraTest() {
    }

    /**
     * Test of sumar method, of class Calculadora.
     */
    @Test
    public void testSumar() {
        System.out.println("sumar");
        double a = 10.0;
        double b = 20.0;
        Calculadora instance = new Calculadora();
        double expResult = 30.0;
        double result = instance.sumar(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of restsr method, of class Calculadora.
     */
    @Test
    public void testRestar() {
        System.out.println("restsr");
        double a = 50.0;
        double b = 5.0;
        Calculadora instance = new Calculadora();
        double expResult = 45.0;
        double result = instance.restar(a, b);
        assertEquals(expResult, result, 0.0);
    }

//    /**
//     * Test of convertirMetrosAMillas method, of class Calculadora.
//     */
//    @Test
//    public void testConvertirMetrosAMillas() {
//        System.out.println("convertirMetrosAMillas");
//        double m = 0.0;
//        Calculadora instance = new Calculadora();
//        double expResult = 0.0;
//        double result = instance.convertirMetrosAMillas(m);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
