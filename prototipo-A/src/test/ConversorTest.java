/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Javi
 */
public class ConversorTest {
    
    public ConversorTest() {
    }
    
    private static final double DELTA = 1e-15;

    /**
     * Test of convertirMetros method, of class Conversor.
     */
    @Test
    public void testConvertirMetros_Caso1_Millas() {
        
        System.out.println("convertirMetros");
        Conversor conversor = new Conversor();
        String unidadDestino;
        double expResult;
        double result;
        
        System.out.println("convertirMetros - Caso de prueba 1 con millas");

        double metros = 10;
        unidadDestino = "millas";
        
        result = conversor.convertirMetros(metros, unidadDestino);
        expResult = metros / 1609;
        
        assertEquals(expResult, result, DELTA);
        
        System.out.println("Resultado esperado: " + expResult);
        System.out.println("Resultado obtenido: " + result);
        System.out.println("");
    }
    
    @Test
    public void testConvertirMetros_Caso2_Pulgadas() {
        
        System.out.println("convertirMetros");
        Conversor conversor = new Conversor();
        String unidadDestino;
        double expResult;
        double result;
        
        System.out.println("convertirMetros - Caso de prueba 2 con pulgadas");

        double metros = 10;
        unidadDestino = "pulgadas";
        
        result = conversor.convertirMetros(metros, unidadDestino);
        expResult = metros * 39.37;
        
        assertEquals(expResult, result, DELTA);
        
        System.out.println("Resultado esperado: " + expResult);
        System.out.println("Resultado obtenido: " + result);
        System.out.println("");
    }
    
    @Test    
    public void testConvertirMetros_Caso3_Pies() {
        
        System.out.println("convertirMetros");
        Conversor conversor = new Conversor();
        String unidadDestino;
        double expResult;
        double result;
        
        System.out.println("convertirMetros - Caso de prueba 3 con pies");

        double metros = 10;
        unidadDestino = "pies";
        
        result = conversor.convertirMetros(metros, unidadDestino);
        expResult = metros * 3.281;
        
        assertEquals(expResult, result, DELTA);
        
        System.out.println("Resultado esperado: " + expResult);
        System.out.println("Resultado obtenido: " + result);
        System.out.println("");
    }
    
    @Test
    public void testConvertirMetros_Caso4_SinUnidad() {
        
        System.out.println("convertirMetros");
        Conversor conversor = new Conversor();
        String unidadDestino;
        double expResult;
        double result;
        
        System.out.println("convertirMetros - Caso de prueba 4 Sin Unidad");

        double metros = 10;
        unidadDestino = "otra";
        
        result = conversor.convertirMetros(metros, unidadDestino);
        expResult = -1;
        
        assertEquals(expResult, result, DELTA);
        
        System.out.println("Resultado esperado: " + expResult);
        System.out.println("Resultado obtenido: " + result);
        System.out.println("");
    }
    
}
