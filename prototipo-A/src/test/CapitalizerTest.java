/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Javi
 */
public class CapitalizerTest {
    
    public CapitalizerTest() {
    }
    
    @Test
    public void testConvertirPrimeraLetraAMayusculas_Caso2_Puntos() {
        
        System.out.println("convertirPrimeraLetraAMayusculas");
        
        String texto;
        Capitalizer capitalizer = new Capitalizer();
        String expResult;
        String result;
        
        System.out.println("convertirPrimeraLetraAMayusculas - Caso de prueba 2 Puntos");
        texto = "...";
        expResult= "...";
        result = capitalizer.convertirPrimeraLetraAMayusculas(texto);
        assertEquals(expResult, result);
        
        System.out.println("Resultado esperado: " + expResult);
        System.out.println("Resultado obtenido: " + result);
        System.out.println("");
    }
    
    @Test
    public void testConvertirPrimeraLetraAMayusculas_Caso3_Palabras() {
        
        System.out.println("convertirPrimeraLetraAMayusculas");
        
        String texto;
        Capitalizer capitalizer = new Capitalizer();
        String expResult;
        String result;
        
        System.out.println("convertirPrimeraLetraAMayusculas - Caso de prueba 3 Palabras");
        texto = "texto de prueba";
        expResult= "Texto De Prueba";
        result = capitalizer.convertirPrimeraLetraAMayusculas(texto);
        assertEquals(expResult, result);
        
        System.out.println("Resultado esperado: " + expResult);
        System.out.println("Resultado obtenido: " + result);
        System.out.println("");
    }
    
}
