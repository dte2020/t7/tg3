/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Javi
 */
public class CalculadoraIT {
    
    public CalculadoraIT() {
    }
    
    @Test
    public void testConvertirMetrosAMillas() {
        System.out.println("convertirMetrosAMillas");
        double m = 1000.0;
        Calculadora instance = new Calculadora();
        double expResult = 1000.0/1609.0;
        double result = instance.convertirMetrosAMillas(m);
        assertEquals(expResult, result, 0.0);
    }
    
}
