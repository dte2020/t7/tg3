/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author Javi
 */
public class Capitalizer {
    
    
    public String convertirPrimeraLetraAMayusculas(String texto) {
        String words[]=texto.split("\\s");  
        String capitalizeWord="";  
        for(String w:words){  
            String first=w.substring(0,1);  
            String afterfirst=w.substring(1);  
            capitalizeWord+=first.toUpperCase()+afterfirst+" ";  
        }  
            return capitalizeWord.trim();  
        }  

    }
    

