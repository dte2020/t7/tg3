/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author Javi
 */
public class Calculadora {
    
    public double sumar(double a, double b){
        return a+b;
    }
    
    public double restar(double a, double b){
        return a-b;
    }
    
    public double convertirMetrosAMillas (double m){
        Conversor conversor = new Conversor();
        return conversor.convertirMetros(m, "millas");
    }
    
}
